<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\FileUpload\InputFile;

class UserController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function info() {
        $me = Telegram::getMe();
        return response()->json(['data' => $me]);
    }

    public function update() {
        $update = Telegram::getUpdates();

        return response()->json(['data' => $update]);
    }

    public function sendMessage() {
        Telegram::sendMessage([
            'chat_id' => '1265444777',
            'text' => 'This is the message'
        ]);

        return response()->json(['message' => 'Message sent']);
    }
    
    public function sendPhoto() {
        $path = asset('storage/image.jfif');

        Telegram::sendPhoto([
            'chat_id' => '1265444777',
            'photo' => \Telegram\Bot\FileUpload\InputFile::create(\file_get_contents($path), 'image.jpg'),
            'caption' => 'This Laptop'
        ]);
        return response()->json(['message' => 'Message sent']);
    }
}
