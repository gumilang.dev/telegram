<?php

namespace App\Http\Controllers;

use App\Mail\TelegramMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Telegram\Bot\Laravel\Facades\Telegram;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['register', 'login', 'confirmToken']]);
    }

    public function register(Request $request) {
        $validation = Validator::make(request()->all(), [
            'name' => 'required|string',
            'password' => 'required|min:6|alpha_num',
            'email' => 'required|email|unique:users,email'
        ]);

        if ($validation->fails()) {
            return response()->json([
                'error' => $validation->errors()
            ]);
        }

        $create = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'status' => 0
        ]);

        return response()->json([
            'data' => $create,
            'message' => 'Register Success',
        ],200);
    }

    public function otp($param) {
        $split = str_split(str_replace(' ', '', strtolower($param)));
        $word = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

        $first = array_search($split[0], $word);

        $random = rand(101, 999);

        $otp = $first . $random;

        $check = User::select('otp')->where('otp', $otp)->first();
        
        if (empty($check)) {
            return $otp;
        } else {
            $random = rand(1000, 9999);

            $otp = $random;
            return $otp;
        }
        
    }

    public function login(Request $request) {
        $validation = Validator::make(request()->all(), [
            'email' => 'required|email',
            'password' => 'required|alpha_num|min:6'
        ]);

        if ($validation->fails()) {
            return response()->json(['error' => $validation->errors()]);
        }

        $check = User::select(['status', 'id'])->where('email', $request->email)->first();

        if (empty($check)) {
            return response()->json(['error' => ['email' => ['User Not registered']]]);
        } else {
            if ($check['status'] == 0) {
                $otp = $this->otp($request->email);
                Mail::to($request->email)->send(new TelegramMail($otp));

                $save = User::where('id', $check->id)->update(['otp' => $otp]);
                return response()->json([
                    'message' => "Please check your email to get the OTP Code",
                    'data' => []
                ],200);
            } else {
                $token = auth()->attempt(request()->all());
                if (!$token) {
                    return response()->json(['data' => 'Unauthorized user'], 401);
                }
    
                return response()->json([
                    'user' => auth()->user(),
                    'token' => $token
                ], 200);
            }

        }

    }

    public function confirmToken(request $request) {
        $validation = Validator::make(request()->all(), [
            'token' => 'required|numeric',
            'email' => 'required|email',
            'password' => 'required|min:6|alpha_num'
        ]);

        if ($validation->fails()) {
            return response()->json(['error' => $validation->errors()]);
        }

        $check = User::select('otp')->where('otp', $request->token)->first();

        if (empty($check)) {
            return response()->json([
                'error' => [
                    'token' => [
                        'Token didnt match'
                    ]
                ]
            ]);
        }

        $update = User::where('otp', $request->token)->update([
            'status' => 1,
            'otp' => null
        ]);

        $token = auth()->attempt(request()->only('email', 'password'));

        return response()->json([
            'message' => 'OTP is correct, and youre login now',
            'data' => $update,
            'token' => $token
        ]);
    } 

    public function info() {
        try {
            $user = auth()->user();
            return response()->json(['data' => $user]);
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            // do something
            return response()->json(['data' => $e]);
        }

    }

    public function logout() {
        $user = auth()->user();
        $id = $user->id;
        
        $update = User::where('id', $id)->update([
            'status' => 0
        ]);
        
        if ($update) {
            // auth()->logout();
            return response()->json(['message' => 'Logout success'], 200);
        } else {
            return response()->json(['message' => 'Bad Request'], 500);
        }

    }
}
