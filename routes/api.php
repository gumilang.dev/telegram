<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/1991904752:AAHfebWTvV9FfAt3Hcu0EuDypws3-ulvOl0/webhook', function () {
    $updates = Telegram::getWebhookUpdates();

    return 'ok';
});

// Route
Route::middleware(['api'])->group(function ($router) {
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/token', [AuthController::class, 'confirmToken'])->name('token');
    Route::post('/info', [AuthController::class, 'info'])->name('info');
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'user'
], function($router) {
    Route::post('/info', [UserController::class, 'info'])->name('user.info');
    Route::post('/update', [UserController::class, 'update'])->name('user.update');
    Route::post('/sendMessage', [UserController::class, 'sendMessage'])->name('user.sendMessage');
    Route::post('/sendPhoto', [UserController::class, 'sendPhoto'])->name('user.sendPhoto');
});
