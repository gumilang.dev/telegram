<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $user = Telegram::getMe();
    $id = $user->getId();

    return response()->json(['data' => $user, 'id' => $id]);
});

Route::get('/update', function() {
    $update = Telegram::getUpdates();

    return response()->json(['data' => $update]);
});

$updates = Telegram::getWebhookUpdates();

// Example of POST Route:
Route::post('/1991904752:AAHfebWTvV9FfAt3Hcu0EuDypws3-ulvOl0/webhook', function () {
    $updates = Telegram::commandsHandler(true);

    return 'ok';
});

Route::get('webupdate', function() {
    $updates = Telegram::getWebhookUpdates();

    return response()->json(['data' => $updates]);
});

Route::get('/info', function() {
    $info = Telegram::getWebhookInfo();

    return response()->json(['info' => $info]);
});
